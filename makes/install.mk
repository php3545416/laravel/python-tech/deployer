create-workdir:
	@mkdir -p $(WORKDIR)

create-volumes: create-database-volume

create-database-volume:
	@mkdir -p $(WORKDIR)/database/mysql
