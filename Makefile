include .env

DOCKER_COMPOSE_PATH = ./docker-compose.yml
DOCKER_COMPOSE_BASE_COMMAND = docker compose -p ${PROJECT_NAME} -f ${DOCKER_COMPOSE_PATH}

run: ## run project
	@${DOCKER_COMPOSE_BASE_COMMAND} up -d

stop: ## stop project
	@${DOCKER_COMPOSE_BASE_COMMAND} stop

restart: ## restart project
	make stop; make run

rebuild-services: ## rebuild services containers defined in command arg
	@for s in $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)); do \
		${DOCKER_COMPOSE_BASE_COMMAND} stop $$s; \
		${DOCKER_COMPOSE_BASE_COMMAND} rm -f $$s; \
		${DOCKER_COMPOSE_BASE_COMMAND} up -d --no-deps --build $$s; \
	done

exec-service: ## exec service
	@for s in $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)); do \
		${DOCKER_COMPOSE_COMMAND} exec $$s bash; \
		break ; \
	done

call-service: ## exec service
	@${DOCKER_COMPOSE_COMMAND} exec ${SERVICE} ${ACTION}

run-services: ## run services
	@for s in $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)); do \
		${DOCKER_COMPOSE_COMMAND} up -d --no-deps --no-build $$s; \
	done

stop-services: ## stop services
	@for s in $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS)); do \
		${DOCKER_COMPOSE_COMMAND} stop $$s; \
	done

include makes/install.mk

